﻿using UnityEngine;
using System.Collections;

public class MoveTexture : MonoBehaviour
{
    public Vector2 speed;

    private Material _mainMaterial;
    private Vector2 _offSet;
    private Vector2 _limit;

    void Start()
    {
        var renderer = this.GetComponent<Renderer>();

        _mainMaterial = Instantiate(renderer.material) as Material;

        renderer.material = _mainMaterial;

        _limit = _mainMaterial.GetTextureScale("_MainTex");
        _limit = new Vector2(Mathf.Abs(_limit.x), Mathf.Abs(_limit.y));
    }

    void Update()
    {
        _offSet += speed * Time.deltaTime;

        if (_offSet.x > _limit.x)
            _offSet.x = 0;

        if (_offSet.y > _limit.y)
            _offSet.y = 0;

        _mainMaterial.SetTextureOffset("_MainTex", _offSet);
    }
}
