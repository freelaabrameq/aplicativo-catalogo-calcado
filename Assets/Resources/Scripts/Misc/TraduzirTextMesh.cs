﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class TraduzirTextMesh : MonoBehaviour
{

    private TextMesh _myText;

    // Use this for initialization
    public void Start()
    {
        _myText = this.GetComponent<TextMesh>();

        var texto = _myText.text;

        if (texto.Contains("#"))
        {
            var substrings = texto.Split('#');

            texto = substrings[0];

            for (int i = 1; i < substrings.Length; i++)
            {
                var miniStrings = substrings[i].Split(' ');

                var index = -1;

                if (int.TryParse(miniStrings[0], out index))
                {
                    if (DataController.DadosEmpresaAtual.itensCatalogo.Count(x => x.id == index) > 0)   
                        miniStrings[0] = DataController.DadosEmpresaAtual.itensCatalogo.First(x => x.id == index).nomes[DataController.IdiomaId];
                    else if (DataController.DadosEmpresas[0].itensCatalogo.Count(x => x.id == index) > 0)
                        miniStrings[0] = DataController.DadosEmpresas[0].itensCatalogo.First(x => x.id == index).nomes[DataController.IdiomaId];
                }

                for (int j = 0; j < miniStrings.Length; j++)
                {
                    texto += miniStrings[j];
                    if (j + 1 < miniStrings.Length)
                        texto += " ";
                }
            }
        }

        if (texto.Contains("$"))
        {
            var substrings = texto.Split('$');

            texto = substrings[0];

            for (int i = 1; i < substrings.Length; i++)
            {
                var miniStrings = substrings[i].Split(' ');

                miniStrings[0] = DataController.DadosApp.Traduzir("$" + miniStrings[0]);

                for (int j = 0; j < miniStrings.Length; j++)
                {
                    texto += miniStrings[j];
                    if (j + 1 < miniStrings.Length)
                        texto += " ";
                }
            }
        }

        _myText.text = texto;

        if (_myText.text.Contains("\\n"))
            _myText.text = _myText.text.Replace("\\n", "\n");
    }

}
