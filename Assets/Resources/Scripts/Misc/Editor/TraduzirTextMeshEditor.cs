﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TraduzirTextMesh))]
public class TraduzirTextMeshEditor : Editor {

    private TraduzirTextMesh _target;

	public void OnEnable()
    {
        _target = (TraduzirTextMesh)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Traduzir Agora"))
        {
            GameController.GetInstance().GetDataController().Load();

            _target.Start();
        }
    }
}
