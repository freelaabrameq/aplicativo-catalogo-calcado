﻿using UnityEngine;
using System.Collections;

public class Trilho : MonoBehaviour 
{
	private Transform _transform;

	public float startFrame;

	void Awake()
	{
		_transform = transform;
	}

	// Use this for initialization
	void Start () 
	{
		_transform.GetComponent<Animation>()["Default Take"].speed 	= 0.01f;
		_transform.GetComponent<Animation>()["Default Take"].time 	= startFrame * 0.165f;
		_transform.GetComponent<Animation>().Play();
	}

	void OnEnable()
	{
		_transform.GetComponent<Animation>()["Default Take"].time 	= startFrame * 0.155f;
	}
	
}