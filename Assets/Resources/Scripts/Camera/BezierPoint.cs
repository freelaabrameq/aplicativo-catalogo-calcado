﻿using System;
using System.Collections;
using UnityEngine;

[Serializable] 
public class BezierPoint 
{
    public int id = -1;

	public Vector3 wayPoint;
    public Vector3 currentWayPoint;
	public Vector3 preControlPoint, currentPreCP;
	public Vector3 posControlPoint, currentPosCP;
	
    public int  objectId = -1;
	public float speed;
    public bool blockOrbit, blockLookAt;
    public Transform target, orbitObject;
}