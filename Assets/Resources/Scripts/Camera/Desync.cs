﻿using UnityEngine;
using System.Collections;

public class Desync : MonoBehaviour 
{
	private Transform _transform;

	public string nameAnimation;
	public float time = -1;

	void Awake()
	{
		_transform = transform;
	}

	// Use this for initialization
	void Start () 
	{
		if( time==-1 )
			_transform.GetComponent<Animation>()[nameAnimation].time = Random.Range( 0f, 5f );
		else
			_transform.GetComponent<Animation>()[nameAnimation].time = time;

		_transform.GetComponent<Animation>().Play();
	}
	
}