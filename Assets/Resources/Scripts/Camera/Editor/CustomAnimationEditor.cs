﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor (typeof (Animation))]
public class CustomAnimationEditor : Editor
{
	AnimationClip m_SampleClip;
	float m_SampleTime = 0.0f;
	List <AnimationClip> m_ClipList;
	List <string> m_ClipNameList;
	
	public List <AnimationClip> ClipList
	{
		get
		{
			if (m_ClipList == null)
			{
				Animation animation;			
				animation = target as Animation;
				
				if (animation != null)
				{
					m_ClipList = new List <AnimationClip> ();
					foreach (AnimationState state in animation)
					{
						m_ClipList.Add (state.clip);
					}
				}
			}
			
			return m_ClipList;
		}
	}
	
	public List <string> ClipNameList
	{
		get
		{
			if (m_ClipNameList == null)
			{
				List <AnimationClip> clipList;
				
				clipList = ClipList;
				
				if (clipList != null)
				{
					m_ClipNameList = new List <string> ();
					foreach (AnimationClip clip in clipList)
					{
						m_ClipNameList.Add (clip.name);
					}
				}
			}
			
			return m_ClipNameList;
		}
	}
	
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();
		
		Animation animation;
		
		animation = target as Animation;
		
		if (animation == null)
		{
			return;
		}
		
		int clipIndex, newClipIndex;
		float newSampleTime;
		
		clipIndex = m_SampleClip == null ? 0 : ClipList.IndexOf (m_SampleClip);
		newClipIndex = EditorGUILayout.Popup (clipIndex, ClipNameList.ToArray ());
		
		if (clipIndex != newClipIndex)
		{
			m_SampleTime = 0.0f;
		}
		
		m_SampleClip = ClipList [newClipIndex];
		
		newSampleTime = EditorGUILayout.Slider ("Sample time", m_SampleTime, 0.0f, m_SampleClip.length);
		
		if (m_SampleTime != newSampleTime)
		{
			m_SampleClip.SampleAnimation (animation.gameObject, newSampleTime);
		}
		
		m_SampleTime = newSampleTime;
	}

}