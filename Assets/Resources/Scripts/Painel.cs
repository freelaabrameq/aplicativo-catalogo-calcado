﻿using UnityEngine;
using System.Collections;

public class Painel : MonoBehaviour {

	public Texture2D[] painel;
	float screenTimer;
	int painelAtual;

	// Use this for initialization
	void Start () {
	
		painelAtual = 0;
		screenTimer = 0f;

	}
	
	// Update is called once per frame
	void Update () {
		if (screenTimer < 5f)
						screenTimer += Time.deltaTime;
				else {
					if(painelAtual == 2) painelAtual = 0;
					else painelAtual++;

					TrocaPainel();

					screenTimer = 0f;
				}
	}

	void TrocaPainel (){
		GetComponent<Renderer>().material.mainTexture = painel [painelAtual];
	}
}
