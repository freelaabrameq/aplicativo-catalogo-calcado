﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using V_Lib;

[CustomEditor(typeof(DataController))]
public class DataControllerInspector : Editor
{
    private DataController _target;
    private bool _empresasNotFold, _appNotFold, _editandoPalavra;

    private string _palavra;
    private string[] _traducoes;

    private List<bool> _palavrasFold; 

    void OnEnable()
    {
        _target = (DataController)target;
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (DataController.DadosEmpresas == null || DataController.DadosApp == null)
        {
            _target.Load();
        }
        else if (DataController.DadosApp.Palavras == null)
        {
            _target.Load();
        }
        else if (DataController.DadosApp.Dicionario == null)
        {
            _target.Load();
        }

        if (_palavrasFold == null)
            _palavrasFold = new List<bool>();

        #region Botões
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Load"))
                _target.Load();
            if (GUILayout.Button("Save"))
                _target.Save();
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Mount"))
        {
            LoadMaquinasEmpresa();
        }

        #endregion

        _empresasNotFold = EditorGUILayout.Foldout(_empresasNotFold, "DADOS EMPRESA: ");

        if (_empresasNotFold)
        {
            GUILayout.BeginVertical("box");
            GUILayout.Space(5);
            {
                for (int x = 0; x < DataController.DadosEmpresas.Count; x++)
                {
                    var data = DataController.DadosEmpresas[x];

                    #region Empresas
                    GUILayout.BeginVertical("box");
                    {
                        data.geralNotFold = EditorGUILayout.Foldout(data.geralNotFold, "EMPRESA " + data.empresaId + " (" + data.empresaNome + "): ");

                        if (data.geralNotFold)
                        {
                            GUILayout.BeginVertical("box");
                            {
                                #region Dados Gerais
                                data.dadosNotFold = EditorGUILayout.Foldout(data.dadosNotFold, "Dados");

                                if (data.dadosNotFold)
                                {
                                    GUILayout.Space(5);

                                    data.empresaId = EditorGUILayout.IntField("Empresa Id: ", data.empresaId);

                                    GUILayout.Space(5);

                                    data.empresaNome = EditorGUILayout.TextField("Nome Empresa: ", data.empresaNome);

                                    if (data.empresaId > 0)
                                    {
                                        GUILayout.Space(5);

                                        data.telefone = EditorGUILayout.TextField("Telefone: ", data.telefone);
                                    }

                                    GUILayout.Space(5);

                                    data.logo = EditorGUILayout.TextField("Logo: ", data.logo);

                                    GUILayout.Space(5);

                                    GUILayout.BeginVertical("box");
                                    {
                                        GUILayout.BeginHorizontal();
                                        {
                                            GUILayout.Label("Site 1:");
                                            
                                            if (GUILayout.Button("C"))
                                                EditorGUIUtility.systemCopyBuffer = data.site1;
                                            GUILayout.Space(5);
                                            if (GUILayout.Button("P"))
                                                data.site1 = EditorGUIUtility.systemCopyBuffer;
                                        }
                                        GUILayout.EndHorizontal();

                                        data.site1 = EditorGUILayout.TextField(data.site1);

                                        GUILayout.BeginHorizontal();
                                        {
                                            GUILayout.Label(data.empresaId > 0 ? "E-Mail:" : "Site 2:");

                                            if (GUILayout.Button("C"))
                                                EditorGUIUtility.systemCopyBuffer = data.site2;
                                            GUILayout.Space(5);
                                            if (GUILayout.Button("P"))
                                                data.site2 = EditorGUIUtility.systemCopyBuffer;
                                        }
                                        GUILayout.EndHorizontal();

                                        data.site2 = EditorGUILayout.TextField(data.site2);
                                    }
                                    GUILayout.EndVertical();

                                    GUILayout.Space(5);
                                }
                                #endregion
                                #region Itens Catálogo

                                data.itensNotFold = EditorGUILayout.Foldout(data.itensNotFold, "Itens");

                                if (data.itensNotFold)
                                {
                                    GUI.backgroundColor = Color.cyan;

                                    GUILayout.BeginVertical("box");
                                    {
                                        if (GUILayout.Button("Add Item"))
                                            data.itensCatalogo.Add(new DataItemCatalogo(data.itensCatalogo.Count));

                                        for (int j = 0; j < data.itensCatalogo.Count; j++)
                                        {
                                            var item = data.itensCatalogo[j];

                                            item.notFold = EditorGUILayout.Foldout(item.notFold, (item.id + 1) + " - " + item.nome);

                                            if (item.notFold)
                                            {
                                                GUILayout.BeginVertical("box");
                                                {
                                                    GUILayout.Space(5);

                                                    item.id = EditorGUILayout.IntField("Item Id:", item.id);

                                                    GUILayout.Space(5);

                                                    item.nome = EditorGUILayout.TextField("Nome Item:", item.nome);

                                                    GUILayout.Space(5);

                                                    if (item.nomes == null)
                                                    {
                                                        item.nomes = new string[3] { item.nome, "", "" };
                                                    }

                                                    GUILayout.BeginVertical("box");
                                                    {
                                                        for (int k = 0; k < DataController.NoIdiomas; k++)
                                                        {
                                                            GUILayout.BeginHorizontal();
                                                            {
                                                                GUILayout.Label(DataController.Idiomas[k] + ":");

                                                                if (GUILayout.Button("C"))
                                                                    EditorGUIUtility.systemCopyBuffer = item.descricao[k];
                                                                GUILayout.Space(5);
                                                                if (GUILayout.Button("P"))
                                                                {
                                                                    item.nomes[k] = EditorGUIUtility.systemCopyBuffer;
                                                                    if (item.nomes[k].EndsWith("\n"))
                                                                    {
                                                                        item.nomes[k] = item.nomes[k].Substring(0, item.nomes[k].Length - 1);
                                                                    }
                                                                }

                                                            }
                                                            GUILayout.EndHorizontal();

                                                            item.nomes[k] = GUILayout.TextField(item.nomes[k]);
                                                        }
                                                    }
                                                    GUILayout.EndVertical();

                                                    GUILayout.Space(5);

                                                    if (data.empresaId > 0)
                                                    {
                                                        item.fabricantes = string.Empty;
                                                        item.fabricantesId = null;
                                                    }
                                                    else
                                                    {
                                                        item.fabricantes = EditorGUILayout.TextField("Fabricantes", item.fabricantes);

                                                        GUILayout.Space(5);
                                                    }

                                                    EditorGUILayout.LabelField("Descrição");

                                                    GUILayout.BeginVertical("box");
                                                    {
                                                        for (int k = 0; k < DataController.NoIdiomas; k++)
                                                        {
                                                            GUILayout.BeginHorizontal();
                                                            {
                                                                GUILayout.Label(DataController.Idiomas[k] + ":");

                                                                if (GUILayout.Button("C"))
                                                                    EditorGUIUtility.systemCopyBuffer = item.descricao[k];
                                                                GUILayout.Space(5);
                                                                if (GUILayout.Button("P"))
                                                                    item.descricao[k] = EditorGUIUtility.systemCopyBuffer;

                                                            }
                                                            GUILayout.EndHorizontal();

                                                            item.descricao[k] = GUILayout.TextArea(item.descricao[k],
                                                                GUILayout.Height(50), GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth * .85F));
                                                        }
                                                    }
                                                    GUILayout.EndVertical();

                                                    GUILayout.Space(5);

                                                    if (data.empresaId > 0)
                                                    {
                                                        EditorGUILayout.LabelField("Videos");

                                                        GUILayout.BeginVertical("box");
                                                        {
                                                            if (GUILayout.Button("Add Video"))
                                                                item.videos.Add("nome_do_video");

                                                            for (int k = 0; k < item.videos.Count; k++)
                                                            {
                                                                item.videos[k] = EditorGUILayout.TextField("Video " + k, item.videos[k]);
                                                            }

                                                            if (GUILayout.Button("Remover Video"))
                                                                item.videos.RemoveAt(item.videos.Count - 1);
                                                        }
                                                        GUILayout.EndVertical();

                                                        GUILayout.Space(5);
                                                    }
                                                    else
                                                    {
                                                        item.videos = null;
                                                    }

                                                    if (GUILayout.Button("Remove Item"))
                                                    {
                                                        data.itensCatalogo.RemoveAt(j);
                                                        GUILayout.EndVertical();
                                                        break;
                                                    }
                                                }
                                                GUILayout.EndVertical();
                                            }
                                        }

                                    }
                                    GUILayout.EndVertical();

                                    GUI.backgroundColor = Color.white;
                                }

                                GUILayout.Space(5);

                                #endregion
                            }
                            GUILayout.EndVertical();
                        }
                    }
                    GUILayout.EndVertical();
                    #endregion
                }
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Add Empresa"))
            {
                if (DataController.DadosEmpresas.Count > 0)
                    DataController.DadosEmpresas.Add(new DataEmpresa(DataController.DadosEmpresas[DataController.DadosEmpresas.Count-1].empresaId + 1));
                else
                    DataController.DadosEmpresas.Add(new DataEmpresa(0));
            }
            GUILayout.Space(5);
            GUILayout.EndVertical();
        }

        
        
        _target.passeioNotFold = EditorGUILayout.Foldout(_target.passeioNotFold, "PASSEIOS: ");

        if (_target.passeioNotFold)
        {
            if (DataController.passeios == null)
                _target.Load();

            #region Passeios
            GUI.backgroundColor = Color.green;
            GUILayout.BeginVertical("box");
            {
                if (GUILayout.Button("Add Item"))
                    DataController.passeios.Add(new DataPasseio(DataController.NoIdiomas));

                for (int j = 0; j < DataController.passeios.Count; j++)
                {
                    DataController.passeios[j].notFoldOut = EditorGUILayout.Foldout(DataController.passeios[j].notFoldOut, DataController.passeios[j].Cena);

                    if (DataController.passeios[j].notFoldOut)
                    {
                        GUILayout.BeginVertical("box");
                        {
                            DataController.passeios[j].Cena = EditorGUILayout.TextField("Cena: ", DataController.passeios[j].Cena);

                            for (int i = 0; i < DataController.NoIdiomas; i++)
                            {
                                DataController.passeios[j].NomeSetor[i] = EditorGUILayout.TextField(DataController.Idiomas[i] + ":", DataController.passeios[j].NomeSetor[i]);
                            }

                            if (GUILayout.Button("Remove"))
                                DataController.passeios.RemoveAt(j);
                        }
                        GUILayout.EndVertical();
                    }
                }
            }
            GUILayout.EndVertical();
            GUI.backgroundColor = Color.white;
                #endregion
        }
        

        _appNotFold = EditorGUILayout.Foldout(_appNotFold, "DADOS APP: ");

        if (_appNotFold)
        {
            #region Dados da aplicação
            GUI.backgroundColor = Color.yellow;

            GUILayout.BeginVertical("box");
            {
                var data = DataController.DadosApp;

                GUILayout.Space(5);

                if (_editandoPalavra)
                {
                    GUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button("Cancelar"))
                        {
                            _editandoPalavra = false;
                        }
                        if (GUILayout.Button("Salvar"))
                        {
                            data.AddPalavra(_palavra, _traducoes);
                            _editandoPalavra = false;
                        }
                    }
                    GUILayout.EndHorizontal();
                }
                else
                {
                    if (GUILayout.Button("Nova Palavra"))
                    {
                        _editandoPalavra = true;
                        _palavra = "";
                        _traducoes = new string[DataController.NoIdiomas];

                        for (int i = 0; i < _palavrasFold.Count; i++)
                        {
                            _palavrasFold[i] = false;
                        }

                        for (int i = 0; i < DataController.NoIdiomas; i++)
                        {
                            _traducoes[i] = "";
                        }
                    }
                }

                if (_editandoPalavra)
                {
                    _palavra = EditorGUILayout.TextField("Nova Palavra:", _palavra);

                    for (int i = 0; i < _traducoes.Length; i++)
                    {
                        _traducoes[i] = EditorGUILayout.TextField(DataController.Idiomas[i] + ":", _traducoes[i]);
                    }
                }
                else
                {
                    if (_palavrasFold.Count != data.Palavras.Count)
                    {
                        _palavrasFold = new List<bool>();

                        for (int i = 0; i < DataController.DadosApp.Palavras.Count; i++)
                        {
                            _palavrasFold.Add(false);
                        }
                    }

                    for (int i = 0; i < data.Palavras.Count; i++)
                    {
                        var estavaFechado = !_palavrasFold[i];

                        _palavrasFold[i] = EditorGUILayout.Foldout(_palavrasFold[i], data.Palavras[i]);

                        if (_palavrasFold[i])
                        {
                            if (estavaFechado)
                            {
                                for (int j = 0; j < data.Palavras.Count; j++)
                                {
                                    _palavrasFold[j] = false;
                                }

                                _palavrasFold[i] = true;

                                _traducoes = new string[DataController.NoIdiomas];

                                for (int j = 0; j < DataController.NoIdiomas; j++)
                                {
                                    _traducoes[j] = data.Traduzir(i, j);
                                }
                            }

                            for (int j = 0; j < _traducoes.Length; j++)
                            {
                                EditorGUILayout.LabelField(DataController.Idiomas[j] + ":" + _traducoes[j]);
                            }
                        }
                    }
                }
            }
            GUILayout.EndVertical();

            GUI.backgroundColor = Color.white;
            #endregion
        }

        EditorUtility.SetDirty(_target);
    }
    private void LoadMaquinasEmpresa()
    {
        var itensGeral = DataController.DadosEmpresas[0].itensCatalogo;

        for (int e = 1; e < DataController.DadosEmpresas.Count; e++)
        { 
            var empresaId = DataController.DadosEmpresas[e].empresaId;
            DataController.DadosEmpresas[e].itensCatalogo = new List<DataItemCatalogo>();

            for (int i = 0; i < itensGeral.Count; i++)
            {
                if (itensGeral[i].fabricantesId.Contains(empresaId))
                {
                    if (empresaId == 3)
                        Debug.Log(itensGeral[i].nome);

                    DataController.DadosEmpresas[e].itensCatalogo.Add(itensGeral[i].XmlSerialize().XmlDeserialize<DataItemCatalogo>());
                }
            }
        }
    }
}
