﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TelaController : MonoBehaviour {

    public static int TelaAtualId { get; set; }

    [HideInInspector]
    public List<TelaBase> TodasTelas;

    public RectTransform rtTop, rtBottom, rtCanvas;
    public Text txtTituloTela, txtNomeEmpresa;
    public Button buttonVoltar;

    void Awake()
    {
        rtCanvas.gameObject.SetActive(true);
    }

    public void Init()
    {
        TodasTelas = new List<TelaBase>();

        for (int i = 0; i < (int)Telas.todas; i++)
        {
            TodasTelas.Add(null);
        }

        SetBtVoltar();
    }
    public void SetBtVoltar(bool useAsReturn = true)
    {

        buttonVoltar.onClick = new Button.ButtonClickedEvent();

        if (useAsReturn)
            buttonVoltar.onClick.AddListener(delegate { MudaTela(TodasTelas[TelaAtualId].telaAnterior); });
        else
            buttonVoltar.onClick.AddListener(delegate { GetComponentInChildren<TelaPasseio>().PasseioEscolhido(GameController.passeioAtual); });
    }
    void Start()
    {
        MudaTela((Telas)TelaAtualId);
    }
    public TelaController RegistraTela(TelaBase tela)
    {
        if (TodasTelas[(int)tela.telaTipo] == null)
            TodasTelas[(int)tela.telaTipo] = tela;
        else
            Debug.Log("Você já cadastrou a " + tela.ToString());

        return this;
    }
    public void MudaTela(int tela)
    {
        if (TelaAtualId > -1)
        {
            if (TodasTelas[TelaAtualId] != null)
            {
                TodasTelas[TelaAtualId].Close();
            }
        }

        TelaAtualId = tela;

        if (TodasTelas[TelaAtualId] != null)
        {
            TodasTelas[TelaAtualId].Open();
        }

        if (TelaAtualId > (int)Telas.TelaMenu)
        {
            if (!rtTop.gameObject.activeSelf)
            {
                rtBottom.gameObject.SetActive(true);
                rtTop.gameObject.SetActive(true);

                txtNomeEmpresa.text = DataController.DadosEmpresaAtual.empresaNome;
                buttonVoltar.GetComponentInChildren<Text>().text = DataController.DadosApp.Traduzir("Voltar");  
            }
        }
        else
        {
            rtBottom.gameObject.SetActive(false);
            rtTop.gameObject.SetActive(false);
        }
    }
    public void MudaTela(Telas tela)
    {
        MudaTela((int)tela);
    }
}
