﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using V_Lib;

public class DataController : MonoBehaviour
{
    #region CONST E READONLY
    public const int NoIdiomas = 3;

    public static readonly string[] Idiomas = new string[NoIdiomas] { "pt-BR", "en-US", "es-ES" };

    public List<string> Palavras { get; private set; }
    #endregion

    private static bool _loaded;

    public static DataEmpresa DadosEmpresaAtual { get; private set; }

    public static DataApp DadosApp { get; private set; }

    public static List<DataPasseio> passeios { get; private set; }

    public static int IdiomaId { get; private set; }

    public int empresaId;

    public static List<DataEmpresa> DadosEmpresas { get; set; }

    public DataItemCatalogo ItemCatalogoAtual { get; private set; }

    public Sprite imgNotFound { get; private set; }

    //public List<CameraPath> passeios { get; private set; }
    [HideInInspector]
    public bool passeioNotFold;

    public void Awake()
    {
        if (!_loaded)
            Load();

        _loaded = true;
    }

    public void Save()
    {
        DadosApp.PrepareToSave();

        for (int i = 0; i < DadosEmpresas.Count; i++)
        {
            XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/DataEmpresas/", "Empresa_" + DadosEmpresas[i].empresaId + ".xml", DadosEmpresas[i].XmlSerialize<DataEmpresa>());
        }

        XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/", "DataApp.xml", DadosApp.XmlSerialize<DataApp>());
        XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/", "DataPasseio.xml", passeios.XmlSerialize<List<DataPasseio>>());
    }
    public void Load()
    {
        //passeios = new List<CameraPath>();
        imgNotFound = Resources.Load("ArtsContents/Generic/Textures/imgNotFound", typeof(Sprite)) as Sprite;

       var textAsset = Resources.Load("Data/DataApp", typeof(TextAsset)) as TextAsset;

        if (textAsset != null)
        {
            DadosApp = textAsset.text.XmlDeserialize<DataApp>();
            DadosApp.Init();
        }
        else
        {
            DadosApp = new DataApp();
            DadosApp.FirstInit();
        }

        //textAsset = Resources.Load("Data/DataPasseio", typeof(TextAsset)) as TextAsset;
        textAsset = Resources.Load("Data/DataPasseios/DataPasseio_" + (empresaId), typeof(TextAsset)) as TextAsset;

        if (textAsset != null)
        {
            passeios = textAsset.text.XmlDeserialize<List<DataPasseio>>();
        }
        else
        {
            passeios = new List<DataPasseio>();
        }

        DadosEmpresas = new List<DataEmpresa>();

        var dataFounded = Resources.LoadAll<TextAsset>("Data/DataEmpresas/");

        for (int i = 0; i < dataFounded.Length; i++)
        {
            DadosEmpresas.Add(dataFounded[i].text.XmlDeserialize<DataEmpresa>());

            DadosEmpresas[i].sprLogo = Resources.Load("ArtsContents/Texture/Logos/" + DadosEmpresas[i].logo, typeof(Sprite)) as Sprite;
        }

        DadosEmpresas = DadosEmpresas.OrderBy(x => x.empresaId).ToList();

        DadosEmpresaAtual = DadosEmpresas.Single(x => x.empresaId == empresaId);
        DadosEmpresaAtual.SetData();
		
		////REMOVE THIS

        //string quebra = "\n";
        //List<string> videos = new List<string>();
        //
        //foreach (var empres in DadosEmpresa.itensCatalogo)
        //{
        //    foreach (var item in empres.videos)
        //    {
        //        videos.Add(item);
        //    }
        //}
        //
        //if (videos.Count > 0)
        //{
        //    List<string> videosUnicos = videos.Distinct().OrderBy(x => x).ToList();
        //    string final = DadosEmpresa.empresaNome + quebra + quebra;
        //        
        //    foreach (var vid in videosUnicos)
        //    {
        //        final += vid + quebra;
        //    }
        //
        //    final += quebra + "TOTAL: " + videosUnicos.Count + quebra + quebra + quebra + quebra;
        //
        //    print(final);
        //}
    }

    public void SetaIdioma(int id)
    {
        //if (DadosEmpresa != null)
        //    DadosEmpresa.SetaIdioma(id);

        IdiomaId = id;
    }
    public Sprite PegaLogo()
    {
        var sprite = Resources.Load("ArtsContents/Texture/Logos/" + DadosEmpresaAtual.logo, typeof(Sprite)) as Sprite;

        if (sprite == null)
            return imgNotFound;
        else
            return sprite;
    }
    public Sprite PegaImagemCatalogo()
    {
        var sprite = Resources.Load("ArtsContents/Texture/IconesMaquinas/" + ItemCatalogoAtual.iconeCatalogo, typeof(Sprite)) as Sprite;

        if (sprite == null)
            return imgNotFound;
        else
            return sprite;
    }
    public Sprite PegaImagemCatalogo(int id)
    {
        var item = DadosEmpresaAtual.itensCatalogo.FirstOrDefault(x => x.id == id);

        var sprite = Resources.Load("ArtsContents/Texture/IconesMaquinas/" + item.iconeCatalogo, typeof(Sprite)) as Sprite;

        if (sprite == null)
            return imgNotFound;
        else
            return sprite;
    }
    public void SetaItemCatalogo(int id)
    {
        //if (DadosEmpresa.itensCatalogo.Count > id)
        //{
            ItemCatalogoAtual = DadosEmpresaAtual.itensCatalogo.First(x => x.id == id);
        //}
        //else
        //{
        //    ItemCatalogoAtual = new DataItemCatalogo();
        //}
    }
}
