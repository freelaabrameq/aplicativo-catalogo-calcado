﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;
using System.IO;

public class ShareController : MonoBehaviour
{
    private string Assunto = "Confira Nosso Catálogo Virtual";
    private string Conteudo = "Olá\nBaixe agora nosso catálogo virtual disponível para Android (https://play.google.com/store/apps/details?id=com.abrameq.calcado) e iOS (https://itunes.apple.com/br/app/brazilian-machinery-calcado/id1148175127?mt=8)!";

    [DllImport("__Internal")]
    private static extern void _TAG_ShareSimpleText(string message);

	public static void ShareSimpleText(string message)
    {
        _TAG_ShareSimpleText(message);
    }
	
    public void ShareButtonClick()
    {

        if (Application.platform == RuntimePlatform.Android)
        {
#if UNITY_ANDROID
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), Assunto);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), Conteudo);
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("startActivity", intentObject);
#endif
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
#if UNITY_IOS
            ShareController.ShareSimpleText(Conteudo);
#endif
        }
        else
        {
            Debug.Log(Assunto + ": " + Conteudo);
        }
    }
}
