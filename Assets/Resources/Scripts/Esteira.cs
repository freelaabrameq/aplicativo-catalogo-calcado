﻿using UnityEngine;
using System.Collections;

public class Esteira : MonoBehaviour
{
	private Transform _trasnform;

	public float scrollSpeedY = -0.5f;
	public float scrollSpeedX = -0.5f;

	void Awake()
	{
		_trasnform = transform;
	}

	void Update () 
	{
		float offsetY = Time.time * scrollSpeedY;
		float offsetX = Time.time * scrollSpeedX;

		_trasnform.GetComponent<Renderer>().material.SetTextureOffset( "_MainTex", new Vector2( offsetX, offsetY) );
	}
}