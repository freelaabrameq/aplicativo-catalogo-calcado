﻿using UnityEngine;
using System.Collections;

public class OperadorEsteira : MonoBehaviour 
{	
	public Transform calcado;
	
	void OnTriggerEnter( Collider p_col )
	{
		if( p_col.name=="Start" )
			calcado.GetComponent<Renderer>().enabled = true;
		else if( p_col.name=="End" )
			calcado.GetComponent<Renderer>().enabled = false;
	}
}