﻿using UnityEngine;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

using System;

[Serializable]
public class DataItemCatalogo
{
    public int id;

    public string nome;

    public string[] nomes;

    public string iconeCatalogo;

    public string modelo3d;

    public string link;

    public string[] descricao;

    public string fabricantes;

    public int[] fabricantesId;

    //[XmlIgnore]
    //public string descricaoAtual;

    public List<string> videos;

    [XmlIgnore]
    public bool notFold;

    public DataItemCatalogo() { }

    public DataItemCatalogo(int id)
    {
        this.id = id;

        this.nome = "Nome Item";

        this.nomes = new string[DataController.NoIdiomas];

        this.descricao = new string[DataController.NoIdiomas];

        this.videos = new List<string>();

        this.notFold = true;

        for (int i = 0; i < DataController.NoIdiomas; i++)
        {
            this.nomes[i] = string.Empty;
            this.descricao[i] = string.Empty;
        }
    }
    public void SetData()
    {
        var tempId = ((this.id + 1) < 10 ? "0" : "") + (this.id + 1);

        this.iconeCatalogo = "TEX_ICON_" + tempId + "_" + (this.nome.Replace(" ", ""));
        this.modelo3d = "PRF_CAT_" + tempId + "_" + (this.nome.Replace(" ", ""));

        if (!string.IsNullOrEmpty(fabricantes))
        {
            var ids = fabricantes.Split(',');

            fabricantesId = new int[ids.Length];

            for (int i = 0; i < ids.Length; i++)
                int.TryParse(ids[i], out fabricantesId[i]);
        }
    }
}
