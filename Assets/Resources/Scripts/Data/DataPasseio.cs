﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

public class DataPasseio
{
    public string Cena;

    public string[] NomeSetor;

    [XmlIgnore]
    public bool notFoldOut;

    public DataPasseio() { }

    public DataPasseio(int idiomas)
    {
        Cena = "SCN_Empresa_X_Setor";
        NomeSetor = new string[idiomas];

        for (int i = 0; i < idiomas; i++)
        {
            NomeSetor[i] = "";
        }
    }
}
