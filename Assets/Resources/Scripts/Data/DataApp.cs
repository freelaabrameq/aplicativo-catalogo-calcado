﻿using UnityEngine;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

using V_Lib;

[System.Serializable]
public class DataApp
{

    [System.Serializable]
    public class DicionarioSerializavel
    {
        public string key;
        public string[] values;
    }

    public List<string> Palavras { get; private set; }

    public List<DicionarioSerializavel> DicionarioSrl { get; private set; }

    [XmlIgnore]
    public Dictionary<KeyValuePair<string, int>, string> Dicionario { get; private set; }

    public void FirstInit()
    {
        Palavras = new List<string>();
        Dicionario = new Dictionary<KeyValuePair<string, int>, string>();
    }

    public void Init()
    {
        Dicionario = new Dictionary<KeyValuePair<string, int>, string>();

        for (int i = 0; i < DicionarioSrl.Count; i++)
        {
            for (int j = 0; j < DataController.NoIdiomas; j++)
            {
                Dicionario.Add(new KeyValuePair<string, int>(DicionarioSrl[i].key, j), DicionarioSrl[i].values[j]);
            }
        }
    }
    public void PrepareToSave()
    {
        DicionarioSrl = new List<DicionarioSerializavel>();

        for (int i = 0; i < Palavras.Count; i++)
        {
            var d = new DicionarioSerializavel();

            d.key = Palavras[i];
            d.values = new string[DataController.NoIdiomas];

            for (int j = 0; j < DataController.NoIdiomas; j++)
            {
                d.values[j] = Traduzir(i, j);             
            }

            DicionarioSrl.Add(d);
        }
    }
    public void AddPalavra(string palavra, string[] traducoes)
    {
        Palavras.Add(palavra);

        Palavras.Sort();

        if (traducoes.Length < DataController.NoIdiomas)
        {
            Debug.LogWarning("A palavra '" + palavra + "' foi cadastrada sem todas as traduções");

            var trd = new List<string>();

            for (int i = 0; i < DataController.NoIdiomas; i++)
            {
                if (traducoes.Length > i)
                    trd.Add(traducoes[i]);
                else
                    trd.Add(palavra);
            }
        }
        else
        {
            for (int i = 0; i < DataController.NoIdiomas; i++)
            {
                Dicionario.Add(new KeyValuePair<string, int>(palavra, i), traducoes[i]);
            }
        }
    }
    public string Traduzir(string palavra)
    {
        var traducao = "";

        if (Dicionario.TryGetValue(new KeyValuePair<string, int>(palavra, DataController.IdiomaId), out traducao))
            return traducao;
        else
            return palavra;
    }
    public string Traduzir(int palavraId)
    {
       return Traduzir(Palavras[palavraId]);
    }
    public string Traduzir(int palavraId, int idioma)
    {
        var traducao = "";

        if (Dicionario.TryGetValue(new KeyValuePair<string, int>(Palavras[palavraId], idioma), out traducao))
            return traducao;
        else
            return Palavras[palavraId];
    }
}
