﻿using UnityEngine;
using System.Xml.Serialization;
using System.Collections.Generic;

[System.Serializable]
public class DataEmpresa {

    public int empresaId;

    public string empresaNome, telefone, logo, site1, site2;

    public List<DataItemCatalogo> itensCatalogo;
    
    [XmlIgnore]
    public bool dadosNotFold, itensNotFold, geralNotFold;
    [XmlIgnore]
    public Sprite sprLogo;

    public DataEmpresa() { }

    public DataEmpresa(int id)
    {
        this.empresaId = id;
        this.empresaNome = "Nome da Empresa";
        this.site1 = "www.site.com.br";
        this.site2 = "email@empresa.com.br";
        this.telefone = "55 (51) 3555-5555";

        this.itensCatalogo = new List<DataItemCatalogo>();

        geralNotFold = true;
        dadosNotFold = true;
    }

    public void SetData()
    {
        for (int i = 0; i < itensCatalogo.Count; i++)
        {
            itensCatalogo[i].SetData();
        }
    }
}
