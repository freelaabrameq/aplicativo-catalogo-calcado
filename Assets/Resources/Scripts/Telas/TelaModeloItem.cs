﻿using UnityEngine;
using System.Collections;

public class TelaModeloItem : TelaBase {

    public bool mouseOverPanel;

    private HolderModeloItem modeloUI;

//    private Vector3 _camDefaultPos;

    private Orbit _orbit;

    void Awake()
    {
        Init();
    }
    public override void Init()
    {
        telaTipo = Telas.TelaModeloItem;
        telaAnterior = Telas.TelaItemCatalogo;

        base.Init();

        base.Open();

        modeloUI = Holder.GetComponent<HolderModeloItem>();
        modeloUI.Init();

        _orbit = modeloUI.trnCamera.GetComponent<Orbit>();

        base.Close();
    }

    void Start()
    {
        if (_wasInitialized)
            return;

        _wasInitialized = true;

        base.Open();

        _orbit.Init(null);


        //modeloUI.btReset.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        //modeloUI.btReset.onClick.AddListener(delegate { this.Reset(); });

        Close();
    }
    void Update()
    {
        //if (this._isOpen)
        //{
        //    _orbit.canOrbit = true;
        //}
    }
    //public void OnHighChange()
    //{
    //    var z = modeloUI.trnCamera.localPosition.z;

    //    modeloUI.trnCamera.localPosition = new Vector3(_camDefaultPos.x, Mathf.Lerp(_camDefaultPos.y - 3, _camDefaultPos.z + 3, modeloUI.slCamY.value), z);
    //}
    public void BtFabricantes()
    {
        _telaController.MudaTela(Telas.TelaFabricantes);
    }
    public void OnRotationChange()
    {

    }

    public void Reset()
    {
        _orbit.Init(modeloUI.goModelo ? modeloUI.goModelo.transform : null);
    }
    public override void Open()
    {
        base.Open();

        if (modeloUI)
        {
            _telaController.txtTituloTela.text = _dataController.ItemCatalogoAtual.nomes[DataController.IdiomaId];

            modeloUI.goVisualizador.SetActive(true);

            //TODO: Destruir os modelos ou só desligá-los???
            if (modeloUI.goModelo != null)
            {
                Destroy(modeloUI.goModelo);
            }

            var model = Resources.Load("ArtsContents/Prefabs/Maquinas/Catalogo/" + _dataController.ItemCatalogoAtual.modelo3d, typeof(GameObject)) as GameObject;

            if (model != null)
            {
                modeloUI.goModelo = (GameObject)Instantiate(model);
                modeloUI.goModelo.transform.SetParent(modeloUI.trnReferencia);
                modeloUI.goModelo.transform.localPosition = Vector3.zero;
                modeloUI.goModelo.layer = modeloUI.trnReferencia.gameObject.layer;
            }

            Reset();

            _orbit.canOrbit = true;
        }
    }

    public override void Close()
    {
        base.Close();

        modeloUI.goVisualizador.SetActive(false);

        if (_orbit)
            _orbit.canOrbit = false;
    }
}
