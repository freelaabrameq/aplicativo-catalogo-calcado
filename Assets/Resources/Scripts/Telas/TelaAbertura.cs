﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TelaAbertura : TelaBase {

    private HolderAbertura aberturaUI;

    public HolderItensIntro itensIntro;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaAbertura;
        telaAnterior = Telas.none;

        base.Init();

        this.Open();

        aberturaUI = Holder.GetComponent<HolderAbertura>();
        aberturaUI.Init();

        this.Close();
    }
    public override void Open()
    {
        base.Open();

        if (aberturaUI)
        {
            itensIntro.holder.SetActive(true);

            itensIntro.txtNomeDaEmpresa.text = DataController.DadosEmpresaAtual.empresaNome;

            itensIntro.imgLogo.sprite = _dataController.PegaLogo();

            itensIntro.txtLink1.text = DataController.DadosEmpresaAtual.site1;
            itensIntro.txtLink2.text = DataController.DadosEmpresaAtual.site2;

            SetaIdioma();
        }

    }
    void Start()
    {
        if (_wasInitialized)
            return;

        _wasInitialized = true;

        #region Seta Botões Idioma
        for (int i = 0; i < DataController.NoIdiomas; i++)
        {
            aberturaUI.buttons[i].name = "ButtonIdioma" + i;
            var id = i;

            aberturaUI.buttons[i].onClick = new Button.ButtonClickedEvent();
            aberturaUI.buttons[i].onClick.AddListener(delegate { ButtonDefineIdioma(id); });
        }
        #endregion
    }

    private void SetaIdioma()
    {
        aberturaUI.txtIdioma.text = DataController.DadosApp.Traduzir("Idioma");
        itensIntro.txtRealizacao.text = DataController.DadosApp.Traduzir("Realização");
        itensIntro.txtApoio.text = DataController.DadosApp.Traduzir("Apoio");
    }

    public void ButtonDefineIdioma(int idiomaId)
    {
        _dataController.SetaIdioma(idiomaId);
        _telaController.MudaTela(Telas.TelaMenu);

        SetaIdioma();
    }
}
