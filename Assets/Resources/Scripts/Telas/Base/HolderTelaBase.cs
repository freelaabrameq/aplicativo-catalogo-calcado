﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderTelaBase : MonoBehaviour {

    [HideInInspector]
    public CanvasGroup Grupo {get; protected set;}

    public virtual void Init()
    {
        Grupo = this.GetComponent<CanvasGroup>();
    }

}
