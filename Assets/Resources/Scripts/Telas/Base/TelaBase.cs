﻿using UnityEngine;
using System.Collections;

public class TelaBase : MonoBehaviour {

    protected bool _wasInitialized;

    protected bool _isAnimating, _isOpen;

    protected TelaController _telaController;
    protected DataController _dataController;

    public GameObject Holder;

    public Telas telaTipo { get; protected set; }

    public Telas telaAnterior { get; protected set; }

    public virtual void Init()
    {
        this._telaController = GameController.GetInstance().GetTelaController().RegistraTela(this);
        this._dataController = GameController.GetInstance().GetDataController();
    }

    public virtual void Open()
    {
        this._isOpen = true;

        if (this.Holder)
            this.Holder.gameObject.SetActive(true);
    }

    public virtual void Close()
    {
        this._isOpen = false;
        if (this.Holder)
            this.Holder.gameObject.SetActive(false);
    }

    public virtual void Return()
    {
        this.Close();

        this._telaController.MudaTela(telaAnterior);
    }
}
