﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class TelaCatalogoEmpresa : TelaBase
{
    private HolderEmpresaCatalogo empresaUI;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaFabricantes;
        telaAnterior = Telas.TelaItemCatalogo;

        base.Init();

        base.Open();

        empresaUI = Holder.GetComponent<HolderEmpresaCatalogo>();
        empresaUI.Init();

        base.Close();
    }

    public override void Open()
    {
        base.Open();

        if (empresaUI)
        {
            if (empresaUI.tempHolder)
                Destroy(empresaUI.tempHolder.gameObject);

            empresaUI.cartaoBase.gameObject.SetActive(false);
            empresaUI.rtHolderCartoes.gameObject.SetActive(true);

            empresaUI.tempHolder = Instantiate(empresaUI.rtHolderCartoes);
            empresaUI.tempHolder.SetParent(empresaUI.rtHolderCartoes.parent);
            empresaUI.tempHolder.localScale = Vector3.one;
            empresaUI.tempHolder.localPosition = Vector3.zero;
            empresaUI.tempHolder.anchoredPosition = Vector3.zero;

            empresaUI.cartaoBase.gameObject.SetActive(true);

            var noCartoes = _dataController.ItemCatalogoAtual.fabricantesId.Length;
            var size = empresaUI.cartaoBase.rectTransform.sizeDelta;

            var holderSize = empresaUI.tempHolder.sizeDelta;
            holderSize.y = 122;

            var pos = -Vector3.up * 20;

            for (int i = 0; i < noCartoes; i++)
            {
                var data = DataController.DadosEmpresas.FirstOrDefault(x => x.empresaId == _dataController.ItemCatalogoAtual.fabricantesId[i]);

                if (data != null)
                {
                    var cartao = Instantiate(empresaUI.cartaoBase);

                    cartao.transform.SetParent(empresaUI.tempHolder.transform);
                    cartao.transform.localScale = empresaUI.cartaoBase.transform.localScale;
                    cartao.transform.localPosition = pos;

                    holderSize.y += (size.y + 20);

                    pos -= Vector3.up * (size.y + 20);

                    cartao.txtNome.text = data.empresaNome;
                    cartao.txtEmail.text = data.site2;
                    cartao.txtSite.text = data.site1;
                    cartao.txtTelefone.text = data.telefone;

                    cartao.imgLogo.sprite = data.sprLogo;
                }
            }

            empresaUI.tempHolder.sizeDelta = holderSize;

            empresaUI.scrollRect.content = empresaUI.tempHolder;
            empresaUI.cartaoBase.gameObject.SetActive(false);
            empresaUI.rtHolderCartoes.gameObject.SetActive(false);
        }
    }
}
