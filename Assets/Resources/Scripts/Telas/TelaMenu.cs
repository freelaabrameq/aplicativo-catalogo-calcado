﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TelaMenu : TelaBase
{
    private HolderMenu menuUI;

    public HolderItensIntro itensIntro;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaMenu;
        telaAnterior = Telas.TelaAbertura;

        base.Init();

        base.Open();

        menuUI = Holder.GetComponent<HolderMenu>();
        menuUI.Init();

        Close();
	}
    void Start()
    {
        if (_wasInitialized)
            return;

        _wasInitialized = true;

        itensIntro.txtNomeDaEmpresa.text = DataController.DadosEmpresaAtual.empresaNome;

        itensIntro.imgLogo.sprite = _dataController.PegaLogo();

        itensIntro.txtLink1.text = DataController.DadosEmpresaAtual.site1;
        itensIntro.txtLink2.text = DataController.DadosEmpresaAtual.site2;

        #region Menu
        menuUI.btCatalogo.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        menuUI.btCatalogo.onClick.AddListener(delegate { _telaController.MudaTela((int)Telas.TelaCatalogo); });

        menuUI.btPasseio.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        menuUI.btPasseio.onClick.AddListener(delegate { _telaController.MudaTela((int)Telas.TelaPasseio); });
        #endregion

        SetaIdioma();
    }
    public override void Open()
    {
        base.Open();

        itensIntro.holder.SetActive(true);

        SetaIdioma();
    }
    private void SetaIdioma()
    {
        menuUI.txtBtCatalogo.text = DataController.DadosApp.Traduzir("Catálogo");
        menuUI.txtBtPasseio.text = DataController.DadosApp.Traduzir("Passeio Virtual");

        itensIntro.txtRealizacao.text = DataController.DadosApp.Traduzir("Realização");
        itensIntro.txtApoio.text = DataController.DadosApp.Traduzir("Apoio");
    }
    public override void Close()
    {

        itensIntro.holder.SetActive(false);

        base.Close();
    }
}
