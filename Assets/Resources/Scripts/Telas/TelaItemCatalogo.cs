﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TelaItemCatalogo : TelaBase
{
    private HolderItemCatalogo itemCatalogoUI;

    private float size;

    public bool _voltandoDoPasseio;
    //private bool _ajustar;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaItemCatalogo;
        telaAnterior = Telas.TelaCatalogo;

        base.Init();

        base.Open();

        itemCatalogoUI = Holder.GetComponent<HolderItemCatalogo>();
        itemCatalogoUI.Init();


        if (_dataController.empresaId > 0)
        {
            itemCatalogoUI.btFabricantes.gameObject.SetActive(false);
            itemCatalogoUI.btVideos.gameObject.SetActive(true);
            itemCatalogoUI.btLink.gameObject.SetActive(true);
        }
        else
        {
            itemCatalogoUI.btFabricantes.gameObject.SetActive(true);
            itemCatalogoUI.btVideos.gameObject.SetActive(false);
            itemCatalogoUI.btLink.gameObject.SetActive(false);
        }

        base.Close();
	}

    public override void Open()
    {
        base.Open();

        if (itemCatalogoUI)
        {
            itemCatalogoUI.txtCaracteristicas.text = DataController.DadosApp.Traduzir("Características");

            _telaController.txtTituloTela.text = _dataController.ItemCatalogoAtual.nomes[DataController.IdiomaId];
            _telaController.txtNomeEmpresa.text = DataController.DadosEmpresaAtual.empresaNome;

            itemCatalogoUI.btVideos.interactable = _dataController.ItemCatalogoAtual.videos.Count > 0;
            itemCatalogoUI.btMaquina.interactable = !string.IsNullOrEmpty(_dataController.ItemCatalogoAtual.modelo3d);
            //itemCatalogoUI.btLink.interactable = !string.IsNullOrEmpty(_dataController.ItemCatalogoAtual.link);
            itemCatalogoUI.txtDescricao.text = _dataController.ItemCatalogoAtual.descricao[DataController.IdiomaId];
            itemCatalogoUI.imgMaquina.sprite = _dataController.PegaImagemCatalogo();

            if (itemCatalogoUI.btVideos.gameObject.activeSelf)
                itemCatalogoUI.btVideos.interactable = _dataController.ItemCatalogoAtual.videos.Count > 0;
            //_ajustar = true;

            if (_voltandoDoPasseio)
            {
                GameController.GetInstance().GetTelaController().SetBtVoltar(false);
            }
        }
    }
    void Update()
    {
        if (size != itemCatalogoUI.txtDescricao.rectTransform.sizeDelta.y)
        {
            //_ajustar = false;

            itemCatalogoUI.rtContainerMovel.gameObject.SetActive(true);

            itemCatalogoUI.rtContainerMovel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _telaController.rtCanvas.sizeDelta.y);

            size = itemCatalogoUI.txtDescricao.rectTransform.sizeDelta.y;

            var pos = itemCatalogoUI.txtDescricao.rectTransform.anchoredPosition;

            pos.y = Mathf.Abs(pos.y);

            if (pos.y + size > _telaController.rtCanvas.sizeDelta.y - _telaController.rtBottom.sizeDelta.y)
            {
                var dif = pos.y + size - (_telaController.rtCanvas.sizeDelta.y - _telaController.rtBottom.sizeDelta.y);

                itemCatalogoUI.rtContainerMovel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, itemCatalogoUI.rtContainerMovel.sizeDelta.y + dif);
            }

            pos.y = -(itemCatalogoUI.rtContainerMovel.sizeDelta.y / 2);

            itemCatalogoUI.rtContainerMovel.anchoredPosition = pos;
        }
    }
    void Start()
    {
        if (_wasInitialized)
            return;

        _wasInitialized = true;

        itemCatalogoUI.btFabricantes.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        itemCatalogoUI.btFabricantes.onClick.AddListener(delegate {
            GameController.GetInstance().GetTelaController().SetBtVoltar(true);
            _telaController.MudaTela((int)Telas.TelaFabricantes); });

        itemCatalogoUI.btVideos.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        itemCatalogoUI.btVideos.onClick.AddListener(delegate {
            GameController.GetInstance().GetTelaController().SetBtVoltar(true);
            _telaController.MudaTela((int)Telas.TelaVideosItem); });

        itemCatalogoUI.btMaquina.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        itemCatalogoUI.btMaquina.onClick.AddListener(delegate {
            GameController.GetInstance().GetTelaController().SetBtVoltar(true);
            _telaController.MudaTela((int)Telas.TelaModeloItem); });

        var site = "http://" + DataController.DadosEmpresaAtual.site1;

        itemCatalogoUI.btLink.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        itemCatalogoUI.btLink.onClick.AddListener(delegate { Application.OpenURL(site); });
    }
}
