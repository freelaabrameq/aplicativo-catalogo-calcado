﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderCartaoEmpresa : MonoBehaviour
{
    public RectTransform rectTransform;

    public Image imgLogo;

    public Text txtNome, txtTelefone, txtSite, txtEmail;

}
