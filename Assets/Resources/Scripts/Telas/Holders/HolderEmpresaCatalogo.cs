﻿using UnityEngine;
using UnityEngine.UI;

public class HolderEmpresaCatalogo : HolderTelaBase
{
    public ScrollRect scrollRect;

    public HolderCartaoEmpresa cartaoBase;

    public RectTransform rtHolderCartoes;

    [HideInInspector]
    public RectTransform tempHolder;
}
