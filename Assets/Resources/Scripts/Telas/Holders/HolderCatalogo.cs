﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderCatalogo : HolderTelaBase
{
    public Button btItemReference;
    public RectTransform rtContainer;

    [HideInInspector]
    public Button[] buttons;
}
