﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderModeloItem : HolderTelaBase
{
    public GameObject goVisualizador;

    [HideInInspector]
    public GameObject goModelo;

    public Transform trnCamera, trnReferencia;

    public RectTransform rtEnableOrbit;
}
