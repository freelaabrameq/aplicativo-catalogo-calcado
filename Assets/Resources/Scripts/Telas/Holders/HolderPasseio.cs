﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderPasseio : HolderTelaBase
{
    public Text txtFrase, txtCarregando;

    public Button btReferenciaPasseio;

    public RectTransform rtContainer, rtCarregando;

    [HideInInspector]
    public Button[] buttons;
}
