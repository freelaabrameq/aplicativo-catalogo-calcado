﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderItemCatalogo : HolderTelaBase
{
    public Button btVideos, btMaquina, btFabricantes, btLink;

    public Image imgMaquina;

    public Text txtCaracteristicas, txtDescricao;

    public RectTransform rtContainerMovel;
}
