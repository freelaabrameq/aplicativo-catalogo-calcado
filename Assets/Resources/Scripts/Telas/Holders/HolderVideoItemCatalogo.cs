﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderVideoItemCatalogo : HolderTelaBase
{
    public GameObject goPanel, blackScreen;

    public RectTransform rtContainer;

    public Button btReferencia;

    [HideInInspector]
    public Button[] buttons;
}
