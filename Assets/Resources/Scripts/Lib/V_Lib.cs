﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Text.RegularExpressions;

/// <summary>
/// 
/// VERSAO STAND_ALONE
/// 
/// Criando um XML
/// 
/// Os dados precisam estar em forma de ums string, para isso, utilize o metodo XMLSerialize
/// 
/// var Dados_Serializados = Nome_Do_Objeto.XmlSerialize();
/// 
/// XmlHelper.CreateXML(Local_do_arquivo, Nome_do_arquivo, Dados_Serializados);
/// 
/// 
/// Carregando um XML
/// 
/// Crie uma string vazia e chame o metodo LoadXML. A string precisa ser criada fora do metodo, pois ela sera utilizada em seguida
/// 
/// var string_referencia = String.Empty;
/// XmlHelper.LoadXML(Local_do_arquivo+Nome_do_arquivo, ref string_referencia);
/// 
/// Eh sempre bom verificar se o arquivo carregado nao gerou uma string nula ou vazia. Apos o teste, deserialize a string para o tipo do objeto que voce quer.
/// 
/// if (!String.IsNullOrEmpty(string_referencia)){
/// 	Nome_Do_Objeto = string_referencia.XmlDeserialize<TIPO_DO_OBJETO>();
/// }
/// 
/// VERSAO WEB EM BREVE!!!
/// 
/// </summary>

namespace V_Lib
{
	
	public static class V_Extension
	{	        
		public static string XmlSerialize<T>(this T obj) where T : class, new()
		{
			if (obj == null)
				return "null object";

            SetEnvironmentVariables();

			XmlSerializer s = new XmlSerializer(typeof(T));
			StringWriter w = new StringWriter();

			s.Serialize(w, obj);
			w.Close();

			return w.ToString();
		}
		
		public static T XmlDeserialize<T>(this string xml) where T : class, new()
		{
			if (xml == null)
				return null;

            SetEnvironmentVariables();

			XmlSerializer s = new XmlSerializer(typeof(T));
			StringReader r = new StringReader(xml);
			
			try 
			{ 
				T obj = (T) s.Deserialize(r);
				r.Close();
				return obj;
			}
			catch (Exception e)
			{
				Debug.Log (e);
				return null;
			}
		}
        public static string FromCurrencyString(this string texto)
        {
            texto = texto.Replace("$", "");
            texto = texto.Replace(".", "");
            texto = texto.Replace(",", ".");

            return texto;
        }
        public static string FromCurrencyStringToInt(this string texto)
        {
            if (texto.Contains("$"))
            {
                texto = texto.Replace("$", "");
                texto = texto.Replace(",", "");

                texto = texto.Replace(".", "");

                texto = texto.Substring(0, texto.Length - 2);
            }
            return texto;
        }
        public static string FromPercentString(this string texto)
        {
            texto = texto.Replace("%", "");
            //texto = texto.Replace(".", "");
            //texto = texto.Replace(",", ".");

            return texto;
        }
        public static float ClearFloat(this float value, int casas = 1)
        {
            return value = ((int)(value * 10 * casas)) / (10F * casas);
        }
        public static string[] ReadXmlString(this string myXml)
        {
            if (string.IsNullOrEmpty(myXml))
                return null;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(myXml);

            var resposta = new string[xmlDoc.DocumentElement.ChildNodes.Count];

            for (int i = 0; i < resposta.Length; i++)
            {
                resposta[i] = xmlDoc.DocumentElement.ChildNodes[i].InnerText;
            }

            return resposta;       
        }
        public static string Encrypt(this string toEncrypt, string key = null)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(string.IsNullOrEmpty(key) ? "6064d9b231ed4279a3eecffa615f3266" : key);
            // 256-AES key
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.PKCS7;
            // better lang support
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(this string toDecrypt, string key = null)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(string.IsNullOrEmpty(key) ? "6064d9b231ed4279a3eecffa615f3266" : key);
            // AES-256 key
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.PKCS7;
            // better lang support
            ICryptoTransform cTransform = rDel.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static bool IsValidEmail(this string strIn)
        {
            var invalid = false;

            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", V_Lib.V_Extension.DomainMapper, RegexOptions.None);
            }
            catch
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase);
            }
            catch
            {
                return false;
            }
        }

        private static void SetEnvironmentVariables()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
            }
        }

        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            
            domainName = idn.GetAscii(domainName);
            
            return match.Groups[1].Value + domainName;
        }
	}

	public class XmlHelper : MonoBehaviour
    {
		public static bool Busy {get; private set;}

		private static string CreateXmlLocal(string fileLocation, string fileName, string data)
		{
#if !UNITY_WEBPLAYER
			Busy = true;

			try{
				StreamWriter w; 
				
				if (!Directory.Exists(fileLocation))
					Directory.CreateDirectory(fileLocation);
				
				FileInfo f = new FileInfo(fileLocation + fileName); 
				
				if(f.Exists) 
				{
					try{
						f.Delete();
					}
					catch (IOException e){
						Busy = false;
						return ("Erro ao tentar deletar o arquivo: " + e.Message);
					}
				}

				w = f.CreateText(); 
				w.Write(data);
				w.Close(); 

//				#if UNITY_EDITOR
//				AssetDatabase.ImportAsset(Application.dataPath + f.Directory + fileName);
//				#endif
				Busy = false;

                Debug.Log("Xml Created at " + f.Directory + "\\" + fileName);

				return ("Xml Created at " + f.Directory+"\\"+fileName);	
			}
			catch (System.Exception e){
				Busy = false;
                Debug.Log("Error: " + e.Message);
				return (e.Message);
			}
#else
            return "Não é possível salvar no modo webplayer";
#endif

		}
		public static string CreateXML(string fileLocation, string fileName, string data) 
		{ 
			return CreateXmlLocal(fileLocation, fileName, data);
		}
        private static string LoadXML(string fileLocation)
        {

            FileInfo f = new FileInfo(fileLocation);
            if (!f.Exists)
            {
                Busy = false;
                throw new IOException("Arquivo não encontrado");
            }

            StreamReader r = File.OpenText(fileLocation);
            string info = r.ReadToEnd();
            r.Close();

            Debug.Log("Xml Loaded from " + fileLocation);

            return info;
        } 
		private static bool LoadXML(string fileLocation, ref string data) 
		{ 

			FileInfo f = new FileInfo(fileLocation); 
			if (!f.Exists){
				Busy = false;
				return false;
			}
			
			StreamReader r = File.OpenText(fileLocation);
			string info = r.ReadToEnd(); 
			r.Close(); 
			data = info; 
			
			Debug.Log("Xml Loaded from "+fileLocation);

			return true;
		} 
    }
}